DROP TABLE APP.orders;
DROP TABLE APP.cartItem;
DROP TABLE APP.users;

DROP TABLE APP.books;

CREATE TABLE APP.books(
   isbn   VARCHAR(20),
   title  VARCHAR(50),
   author VARCHAR(50),
   price  FLOAT,
   copies INT,
   PRIMARY KEY (isbn)
);

INSERT INTO APP.books
VALUES ('10-01', 'Java for dummies', 'Tan Ah Teck', 11.11, 11);
INSERT INTO APP.books
VALUES ('10-02', 'More Java for dummies', 'Tan Ah Teck', 22.22, 22);
INSERT INTO APP.books
VALUES ('10-03', 'More Java for more dummies', 'Mohammad Ali', 33.33, 33);
INSERT INTO APP.books
VALUES ('10-04', 'A Cup of Java', 'Kumar', 44.44, 44);
INSERT INTO APP.books
VALUES ('10-05', 'A Teaspoon of Java', 'Kevin Jones', 55.55, 55);

CREATE TABLE APP.users (
   id        INT GENERATED ALWAYS AS IDENTITY,
   name      VARCHAR(50),
   email     VARCHAR(100),
   address   VARCHAR(200),
   biography VARCHAR(200),
   type      INT,
   PRIMARY KEY(id));

INSERT INTO APP.users (name, email, address, type)
VALUES ('Albert Einstein', 'albert.einstein@ias.edu', 'Office 9, IAS, Princeton, USA', 1);
INSERT INTO APP.users (name, email, address, type)
VALUES ('James Bond', 'jbond@ghcq.uk', 'Queen Street 9, Chelsea 338, London', 1);
INSERT INTO APP.users (name, email, biography, type)
VALUES ('Ian Fleming', 'ian@fleming.com', 'journalist -> spy -> author', 2);
INSERT INTO APP.users (name, email, biography, type)
VALUES ('Stephen Hawking', 'stephen@blackhole.org', 'RIP', 2);

CREATE TABLE APP.cartItem (
   customerID INT,
   isbn       VARCHAR(20),
   quantity   INT,
   PRIMARY KEY (customerID,isbn),
   FOREIGN KEY (customerID) REFERENCES APP.users (id),
   FOREIGN KEY (isbn) REFERENCES APP.books (isbn)
);

CREATE TABLE APP.orders (
   id         INT,
   isbn       varchar(20),
   customerID INT,
   quantity   INT,
   order_date DATE,
   PRIMARY KEY (id),
   FOREIGN KEY (customerID) REFERENCES APP.users (id),
   FOREIGN KEY (isbn) REFERENCES APP.books (isbn)
);