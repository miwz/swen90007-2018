<%-- Cart Page  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page session="true"
        import="au.edu.unimelb.cis.swen90007.bookshop.Session, au.edu.unimelb.cis.swen90007.bookshop.domain.Book" %>
<%@ page import="au.edu.unimelb.cis.swen90007.bookshop.domain.ShoppingCart" %>
<%@ page import="java.util.Map" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='stylesheet' href='../resources/bootstrap.min.css'/>
    <link rel='stylesheet' href='../style.css'/>
    <title>E-BookShop Shopping Cart</title>
</head>
<body>
<h2>E-Bookshop - Shopping Cart</h2>
<hr/>
<br/>

<p><strong>You have the following book(s) in your shopping cart:</strong></p>

<div class='container'>
    <table class='table table-bordered table-striped'>
        <tr>
            <th>Title</th>
            <th>Author</th>
            <th>Price</th>
            <th>Quantity</th>
        </tr>
        <%
        int customerId = Session.getUser().getId();
        ShoppingCart cart = ShoppingCart.getCartOf(customerId);
        if (cart != null) {
           for(Book bookInCart:cart.getBookCopies().keySet()) {
    %>
    <tr>
        <td><%= bookInCart.getTitle()%></td>
        <td><%= bookInCart.getAuthor()%></td>
        <td align="right">$<%= bookInCart.getPrice()%></td>
        <td align="right"><%= cart.getBookCopies().get(bookInCart)%></td>
    </tr>
    <%
            }//for

        if (cart.getBookCopies().size() > 0) {
    %>
        <tr>
            <th align="right" colspan="2">Total</th>
            <th align="right">$<%= cart.getBookCopies().entrySet().stream()
                    .map((entry) -> entry.getKey().getPrice() * entry.getValue())
                    .reduce(Float::sum)
                    .get()
            %>
            </th>
            <th align="right"><%= cart.getBookCopies().entrySet().stream()
                    .map(Map.Entry::getValue)
                    .reduce(Integer::sum)
                    .get()
            %>
            </th>
        </tr>
        <%
            } // if
        }
    %>
</table></div>
<br />

<div class='container'>
    <table class='table table-bordered table-striped'>
        <tr>
        <th>Ship to: </th><td><%= Session.getUser().getName()%></td></tr>
        <tr><th>Address: </th><td><%= Session.getUser().getAddress()%></td></tr>


        </table>
    </div>
<br />
<form name="checkoutForm" action="cart" method="POST">
    <input type="submit" value="place the order">
</form>
<a href="shop"> Click here to buy more books!</a>
</body>
</html>