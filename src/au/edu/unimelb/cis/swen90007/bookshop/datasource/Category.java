package au.edu.unimelb.cis.swen90007.bookshop.datasource;

public enum Category {

    DRAMA, CRIME, NOVEL
}
