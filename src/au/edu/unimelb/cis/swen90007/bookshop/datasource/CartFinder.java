package au.edu.unimelb.cis.swen90007.bookshop.datasource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class CartFinder {

    private final static String findCartOfUser =
            "SELECT * " +
                    "  from APP.cartItem " +
                    "  WHERE customerID = ?";

    private final static String finditemOfUser =
            "SELECT * " +
                    "  from APP.cartItem " +
                    "  WHERE customerID = ? AND isbn = ?";


    public List<CartItemGateway> findCartOf(int id) {
        List<CartItemGateway> result = new LinkedList<>();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = DBConnection.prepare(findCartOfUser);
            findStatement.setInt(1, id);
            rs = findStatement.executeQuery();
            while (rs.next()) {

                result.add(CartItemGateway.load(rs));
            }

        } catch (SQLException e) {
        }
        return result;
    }

    public CartItemGateway findItem(int userId, String bookId) {
        CartItemGateway result = null;
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = DBConnection.prepare(finditemOfUser);
            findStatement.setInt(1, userId);
            findStatement.setString(2, bookId);
            rs = findStatement.executeQuery();
            while (rs.next()) {

                result = CartItemGateway.load(rs);
            }

        } catch (SQLException e) {
        }
        return result;
    }
}
