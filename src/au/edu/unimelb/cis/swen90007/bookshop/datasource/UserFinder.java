package au.edu.unimelb.cis.swen90007.bookshop.datasource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserFinder {

    private final static String findUserWithID =
            "SELECT * " +
                    "  from APP.users " +
                    "  WHERE id = ?";

    private final static String findUserWithEmail =
            "SELECT * " +
                    "  from APP.users " +
                    "  WHERE email = ?";


    public UserGateway findUserByEmail(String email) {
        UserGateway result = null;
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = DBConnection.prepare(findUserWithEmail);
            findStatement.setString(1, email);
            rs = findStatement.executeQuery();
            while (rs.next()) {
                result = CustomerGateway.load(rs);
            }

        } catch (SQLException e) {
        }
        return result;
    }

    public UserGateway findUserById(int id) {
        UserGateway result = null;
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = DBConnection.prepare(findUserWithID);
            findStatement.setInt(1, id);
            rs = findStatement.executeQuery();
            while (rs.next()) {
                result = UserGateway.load(rs);
            }

        } catch (SQLException e) {
        }
        return result;
    }

}
