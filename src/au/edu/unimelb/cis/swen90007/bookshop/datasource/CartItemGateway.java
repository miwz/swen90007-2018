package au.edu.unimelb.cis.swen90007.bookshop.datasource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class CartItemGateway {
    private int customerID;
    private String isbn;
    private int quantity;

    private static final String updateQuantityString =
            "UPDATE APP.cartItem " +
                    "  set quantity = ? " +
                    "  where customerID = ? AND isbn = ?";

    private static final String insertStatementString =
            "INSERT INTO APP.cartItem VALUES (?, ?, ?)";

    public CartItemGateway(int customerID, String isbn, int quantity) {
        this.customerID = customerID;
        this.isbn = isbn;
        this.quantity = quantity;
    }


    public int getId() {
        return customerID;
    }

    public void setId(int customerID) {
        this.customerID = customerID;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void updateQuantity() {
        PreparedStatement updateStatement = null;
        try {
            updateStatement = DBConnection.prepare(updateQuantityString);
            updateStatement.setInt(1, quantity);
            updateStatement.setInt(2, customerID);
            updateStatement.setString(3, isbn);
            updateStatement.execute();
        } catch (Exception e) {

        }
    }

    public  int insert() {
        PreparedStatement insertStatement = null;
        try {
            insertStatement = DBConnection.prepare(insertStatementString);
            insertStatement.setInt(1, customerID);
            insertStatement.setString(2, isbn);
            insertStatement.setInt(3, quantity);

            insertStatement.execute();
            Registry.addCartItemGateway(this);
        } catch (SQLException e) {
        }
        return getId();
    }

    public static CartItemGateway load(ResultSet rs) throws SQLException {
        int customerId = rs.getInt(1);
        String isbnArg = rs.getString(2);
        CartItemGateway result = Registry.getCartItemGateway(Objects.hash(customerId, isbnArg));
        if (result != null) {
            return result;
        }
        int quantityArg = rs.getInt(3);
        result = new CartItemGateway(customerId, isbnArg, quantityArg);
        Registry.addCartItemGateway(result);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartItemGateway that = (CartItemGateway) o;
        return customerID == that.customerID &&
                isbn.equals( that.isbn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerID, isbn);
    }
}
