package au.edu.unimelb.cis.swen90007.bookshop.datasource;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class UserGateway {

    protected int id;

    protected String email;

    protected String name;

    public UserGateway(int id, String email, String name) {
        this.id = id;
        this.email = email;
        this.name = name;
    }

    public UserGateway(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    static UserGateway load(ResultSet rs) throws SQLException {
        int id = rs.getInt(1);
        UserGateway result = Registry.getUser(id);
        if (result != null)
            return result;

        int type = rs.getInt(6);
        if (type == 1) {
            result = CustomerGateway.load(rs);
        } else if (type == 2) {
            result = AuthorGateway.load(rs);
        }
        Registry.addUser(result);
        return result;
    }
}
