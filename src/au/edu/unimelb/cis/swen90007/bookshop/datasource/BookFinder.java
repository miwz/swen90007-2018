package au.edu.unimelb.cis.swen90007.bookshop.datasource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookFinder {
	
	private final static String findStatementString =
	         "SELECT * " +
	         "  from APP.books " +
					 "  WHERE isbn = ?";
	private static final String findAvailableBooksStatement =
			"SELECT * from APP.books WHERE copies > 0";

	public BookGateway find(String isbn) {
		BookGateway result = Registry.getBook(isbn);
	      if (result  != null) 
	    	  return result;
	      PreparedStatement findStatement = null;
	      ResultSet rs = null;
	      try {
	         findStatement = DBConnection.prepare(findStatementString);
			  findStatement.setString(1, isbn);
	         rs = findStatement.executeQuery();
	         rs.next();
	         result = BookGateway.load(rs);
	          
	      } catch (SQLException e) {
	      }
	      return result;
	   }

    public List<BookGateway> findAvailableBooks() {
	      List<BookGateway> result = new ArrayList<>();
	      try {

			  PreparedStatement stmt = DBConnection.prepare(findAvailableBooksStatement);

			  ResultSet rs = stmt.executeQuery();
			  while (rs.next()) {
				  result.add(BookGateway.load(rs));
			  }

	      } catch (SQLException e) {
	      
	      }
	      return result;
	   }

}
