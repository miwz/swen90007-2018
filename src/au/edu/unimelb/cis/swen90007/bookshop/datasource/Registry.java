package au.edu.unimelb.cis.swen90007.bookshop.datasource;

import java.util.HashMap;
import java.util.Map;

public class Registry {
    private static Map<String, BookGateway> bookGateway = new HashMap<>();
    private static Map<Integer, CartItemGateway> cartItemGateway = new HashMap<>();
    private static Map<Integer, UserGateway> userGateway = new HashMap<>();

    public static BookGateway getBook(String isbn) {
        return bookGateway.get(isbn);
	}

	public static void addBook(BookGateway bookGateway) {
        Registry.bookGateway.put(bookGateway.getIsbn(), bookGateway);
    }

    public static CartItemGateway getCartItemGateway(int id) {
        return cartItemGateway.get(id);
    }

    public static void addCartItemGateway(CartItemGateway cartItemGateway) {
        Registry.cartItemGateway.put(cartItemGateway.hashCode(), cartItemGateway);
    }

    public static void addUser(UserGateway userGateway) {
        Registry.userGateway.put(userGateway.getId(), userGateway);
    }

    public static UserGateway getUser(int id) {
        return userGateway.get(id);
    }
}
