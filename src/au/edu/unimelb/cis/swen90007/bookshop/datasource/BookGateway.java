package au.edu.unimelb.cis.swen90007.bookshop.datasource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BookGateway {
    private String isbn;
	 private String  title;
	 private String  author;
	 private float  price;
    private int copies;
	 private static final String updateStatementString =
	         "UPDATE APP.books " +
                     "  set title = ?, author = ?, price = ?, copies = ? " +
                     "  where isbn = ?";
	
	 private static final String insertStatementString =
	         "INSERT INTO APP.books VALUES (?, ?, ?, ?, ?)";


    public BookGateway(String isbn, String title, String author, float price, int copies) {
		super();
        this.isbn = isbn;
		this.title = title;
		this.author = author;
		this.price = price;
        this.copies = copies;
    }

	public BookGateway() {
	}

    public int getCopies() {
        return copies;
    }

    public void setCopies(int copies) {
        this.copies = copies;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
   public void update() {
      PreparedStatement updateStatement = null;
      try {
         updateStatement = DBConnection.prepare(updateStatementString);
         updateStatement.setString(1, title);
         updateStatement.setString(2, author);
         updateStatement.setFloat(3, price);
          updateStatement.setInt(4, copies);
          updateStatement.setString(5, isbn);

         updateStatement.execute();
      } catch (Exception e) {
    
      }
   }

    public String insert() {

       PreparedStatement insertStatement = null;
      try {
         insertStatement = DBConnection.prepare(insertStatementString);
          insertStatement.setString(1, isbn);
         insertStatement.setString(2, title);
         insertStatement.setString(3, author);
         insertStatement.setFloat(4, price);
          insertStatement.setInt(5, copies);

         insertStatement.execute();


          Registry.addBook(this);
      } catch (SQLException e) {
      }
        return getIsbn();

   }

    public static BookGateway load(ResultSet rs) throws SQLException {
        String id = rs.getString(1);
        BookGateway result = Registry.getBook(id);
        if (result != null)
            return result;
        String titleArg = rs.getString(2);
        String authorArg = rs.getString(3);
        int priceArg = rs.getInt(4);
        int qtyArg = rs.getInt(5);
        result = new BookGateway(id, titleArg, authorArg, priceArg, qtyArg);
        Registry.addBook(result);
        return result;
    }
}
