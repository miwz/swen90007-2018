package au.edu.unimelb.cis.swen90007.bookshop.controller;

import au.edu.unimelb.cis.swen90007.bookshop.Session;
import au.edu.unimelb.cis.swen90007.bookshop.domain.Book;
import au.edu.unimelb.cis.swen90007.bookshop.domain.ShoppingCart;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/shop")
public class ShoppingControllerServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    public void init(ServletConfig conf) throws ServletException {
        super.init(conf);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String view = "/views/shopping.jsp";
        ServletContext servletContext = getServletContext();
        RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(view);
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        String bookID = request.getParameter("bookID");
        Book book = Book.getBook(bookID);

        if (book != null) {
            int customerId = Session.getUser().getId();
            ShoppingCart cart = ShoppingCart.getCartOf(customerId);
            cart.addBook(book, 1, customerId);
        }

        String view = "/views/shopping.jsp";
        ServletContext servletContext = getServletContext();
        RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(view);
        requestDispatcher.forward(request, response);
    }
}
