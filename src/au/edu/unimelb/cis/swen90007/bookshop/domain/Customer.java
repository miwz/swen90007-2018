package au.edu.unimelb.cis.swen90007.bookshop.domain;

import java.util.List;

public class Customer extends User {

    private String address;

    private List<Order> orders;

    private ShoppingCart cart;

    public Customer(String email, String name, String address, List<Order> orders, ShoppingCart cart) {
        super(email, name);
        this.address = address;
        this.orders = orders;
        this.cart = cart;
    }

    public Customer(int customerId, String email, String name, String address, List<Order> orders, ShoppingCart cart) {
        super(customerId, email, name);
        this.address = address;
        this.orders = orders;
        this.cart = cart;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }

}
