package au.edu.unimelb.cis.swen90007.bookshop.domain;

import au.edu.unimelb.cis.swen90007.bookshop.datasource.CartFinder;
import au.edu.unimelb.cis.swen90007.bookshop.datasource.CartItemGateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCart {

    private Map<Book, Integer> bookCopies;

    public ShoppingCart() {
        this.bookCopies = new HashMap<>();
    }

    public Map<Book, Integer> getBookCopies() {
        return bookCopies;
    }

    public void setBookCopies(Map<Book, Integer> bookCopies) {
        this.bookCopies = bookCopies;
    }

    public void addBook(Book book, int qty, int customerID) {
        int copy = qty;
        ShoppingCart currentCart = getCartOf(customerID);
        setBookCopies(currentCart.getBookCopies());

        CartFinder finder = new CartFinder();
        CartItemGateway cartRecord = finder.findItem(customerID, book.getIsbn());

        if(bookCopies.containsKey(book)){
            //update the qyt only
            cartRecord.setQuantity(bookCopies.get(book) + copy);

            cartRecord.updateQuantity();


        }else {
            //insert new book to the cart with qyt = 1
            cartRecord = new CartItemGateway(customerID, book.getIsbn(), copy);
            cartRecord.insert();


        }
        book.setCopies(book.getCopies() - copy);
        Book.updateBook(book);




    }


    public static ShoppingCart getCartOf(int userID) {
        CartFinder finder = new CartFinder();
        List<CartItemGateway> cartRecords = finder.findCartOf(userID);

        ShoppingCart result = new ShoppingCart();
        Map<Book, Integer> bookCopies = new HashMap<>();

        for (CartItemGateway cg : cartRecords) {
            Book book = Book.getBook(cg.getIsbn());
            int qyt = cg.getQuantity();

            if(bookCopies.containsKey(book)) {
                qyt = qyt + bookCopies.get(book);
            }
            bookCopies.put(book, qyt);
        }
        result.setBookCopies(bookCopies);
        return result;
    }




}

