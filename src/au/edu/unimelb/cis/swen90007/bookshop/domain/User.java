package au.edu.unimelb.cis.swen90007.bookshop.domain;

import au.edu.unimelb.cis.swen90007.bookshop.datasource.AuthorGateway;
import au.edu.unimelb.cis.swen90007.bookshop.datasource.CustomerGateway;
import au.edu.unimelb.cis.swen90007.bookshop.datasource.UserFinder;
import au.edu.unimelb.cis.swen90007.bookshop.datasource.UserGateway;

import java.util.LinkedList;

public abstract class User {

    protected int id;

    private String email;

    private String name;

    public User(int id, String email, String name) {
        this.id = id;
        this.email = email;
        this.name = name;
    }

    public User(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public static User getUser(int userId) {
        UserFinder finder = new UserFinder();
        UserGateway record = finder.findUserById(userId);
        User result = null;
        if (record != null) {
            if (record instanceof CustomerGateway) {
                // TODO we don't initialise orders and cart for now
                result = new Customer(record.getId(), record.getEmail(),
                        record.getName(), ((CustomerGateway) record).getAddress(), new LinkedList<>(), new ShoppingCart());
            } else if (record instanceof AuthorGateway) {
                result = new Author(record.getId(), record.getEmail(), record.getName(),
                        ((AuthorGateway) record).getBiography());
            }
        }
        return result;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getAddress(){
        return getAddress();

    }
}
