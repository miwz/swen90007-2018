package au.edu.unimelb.cis.swen90007.bookshop.domain;

import au.edu.unimelb.cis.swen90007.bookshop.datasource.BookFinder;
import au.edu.unimelb.cis.swen90007.bookshop.datasource.BookGateway;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Book {

	private String isbn;

    private String author;

    private String title;
   
    private float price;
    
    private int copies;

    public Book(String isbn, String author, String title, float price, int copies) {
        this.isbn = isbn;
        this.author = author;
        this.title = title;
        this.price = price;
        this.setCopies(copies);
    }

	public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getCopies() {
		return copies;
	}

	public void setCopies(int copies) {
		this.copies = copies;

	}

    public static Book getBook(String isbn) {
        BookFinder finder = new BookFinder();
        BookGateway br = finder.find(isbn);
        return (br == null) ? null : new Book(String.valueOf(br.getIsbn()), br.getAuthor(),
                br.getTitle(), br.getPrice(), br.getCopies());
    }


    public static List<Book> getAllAvailableBooks() {
        BookFinder finder = new BookFinder();
        List<Book> result = new ArrayList<Book>();
        List<BookGateway> booksRecords = finder.findAvailableBooks();

        for (BookGateway br : booksRecords) {
            Book book = new Book(String.valueOf(br.getIsbn()), br.getAuthor(), br.getTitle(),
                    br.getPrice(), br.getCopies());
            result.add(book);
        }
        return result;
    }

    public static void updateBook(Book book) {
        BookFinder finder = new BookFinder();
        BookGateway br = finder.find(book.getIsbn());
        br.setCopies(book.getCopies());
        br.setAuthor(book.getAuthor());
        br.setPrice(book.getPrice());
        br.setTitle(book.getTitle());
        br.update();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(isbn, book.isbn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn);
    }
}
