package au.edu.unimelb.cis.swen90007.bookshop.domain;

import au.edu.unimelb.cis.swen90007.bookshop.datasource.Category;

import java.util.List;

public class Author extends User {

    private String biography;

    // @student How would you represent this relationship in your SQL schema?
    private List<Book> published;

    // @student How would you represent this relationship in your SQL schema?
    private Category specialty;

    public Author(int id, String email, String name, String biography) {
        super(id, email, name);
        this.biography = biography;
    }

    public Author(String email, String name, String biography) {
        super(email, name);
        this.biography = biography;
    }
}
