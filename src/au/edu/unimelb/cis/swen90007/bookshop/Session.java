package au.edu.unimelb.cis.swen90007.bookshop;

import au.edu.unimelb.cis.swen90007.bookshop.domain.User;

public class Session {

    private static User user;

    // TODO in the future we will get the currenly logged-in user from the authentication context
    public static User getUser() {
        if (Session.user == null) {
            //mock (customer) user with ID 1 in DB
            Session.user = User.getUser(1);
        }
        return Session.user;
    }
}
